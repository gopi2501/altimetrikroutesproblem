package com.altimetrik.model;

public class Route {

	String cityA;
	String cityB;
	boolean routeExists;
	public String getCityA() {
		return cityA;
	}
	public void setCityA(String cityA) {
		this.cityA = cityA;
	}
	public String getCityB() {
		return cityB;
	}
	public void setCityB(String cityB) {
		this.cityB = cityB;
	}
	public boolean isRouteExists() {
		return routeExists;
	}
	public void setRouteExists(boolean routeExists) {
		this.routeExists = routeExists;
	}
	public Route(String cityA, String cityB, boolean routeExists) {
		super();
		this.cityA = cityA;
		this.cityB = cityB;
		this.routeExists = routeExists;
	}
	public Route(String cityA, String cityB) {
		super();
		this.cityA = cityA;
		this.cityB = cityB;
	}
	@Override
	public String toString() {
		return "Route [cityA=" + cityA + ", cityB=" + cityB + ", routeExists=" + routeExists + "]";
	}
	
	
}
