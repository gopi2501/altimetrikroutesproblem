package com.altimetrik.springboot.microservices.AltimetrikSpringBootMicroservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@ComponentScan("com.altimetrik.*")
@Configuration
@EnableAutoConfiguration
public class AltimetrikSpringBootMicroservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AltimetrikSpringBootMicroservicesApplication.class, args);
		System.out.println("Server started");
	}

}

