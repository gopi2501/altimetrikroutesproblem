package com.altimetrik.router.dao;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

@Repository
public class RouteDao {
	static int[][] characterArrayGrid;
	static ArrayList<Integer> storeOfChar;
	public RouteDao() {
		characterArrayGrid = new int[6][6];
		storeOfChar = new ArrayList();
	}

	public static String readAJsonfile() throws IOException {
		File resource = new ClassPathResource("routes.json").getFile();
		String routesAvailableAmongCities = new String(Files.readAllBytes(resource.toPath()));
		System.out.println("data from the json file is " + routesAvailableAmongCities);
		return routesAvailableAmongCities;
	}

	public boolean routeExists(String cityOne, String cityTwo) throws IOException, JSONException {
		System.out.println("Route exists methdo in Dao");
		// http://frugalisminds.com/spring/load-file-classpath-spring-boot/

		JSONObject jsonObject = new JSONObject(this.readAJsonfile());
		// Object routesKeyValuePairs = jsonObject.getJSONObject("routes");
		JSONArray requestedTwoCities = new JSONArray();
		requestedTwoCities.put(0, cityOne);
		requestedTwoCities.put(1, cityTwo);

		JSONArray requestedTwoCitiesRev = new JSONArray();
		requestedTwoCitiesRev.put(0, cityTwo);
		requestedTwoCitiesRev.put(1, cityOne);

		System.out.println(requestedTwoCities.toString());

		for (int i = 0; i < jsonObject.getJSONArray("routes").length(); i++) {
			// System.out.println("Heyy i just found a route. Heyy i just found a route.
			// Heyy i just found a route. Heyy i just found a route. Heyy i just found a
			// route. Heyy i just found a route");
			System.out.println(jsonObject.getJSONArray("routes").get(i));
			if (jsonObject.getJSONArray("routes").get(i).toString().equals(requestedTwoCities.toString())
					|| jsonObject.getJSONArray("routes").get(i).toString().equals(requestedTwoCitiesRev.toString())) {
				System.out.println("Route exists among the request");
				return true;
			} else
				return false;
		}
		return false;
	}

	public boolean ifcomplexRouteExists(String cityOne, String cityTwo) throws JSONException, IOException {
		Map<Character, Integer> characterIndex = new HashMap();
		characterIndex.put('A', new Integer(0));
		characterIndex.put('B', new Integer(1));
		characterIndex.put('C', new Integer(2));
		characterIndex.put('D', new Integer(3));
		characterIndex.put('E', new Integer(4));
		characterIndex.put('F', new Integer(5));

		//int[][] characterArrayGrid = new int[6][6];
		// JSONObject jsonObject = new JSONObject(this.readAJsonfile());

		String tempStoreOfData = " {\"routes\": [[\"A\",\"B\"],[\"C\",\"D\"],[\"D\",\"A\"],[\"E\",\"F\"]]}";
		JSONObject jsonObject = new JSONObject(tempStoreOfData);

		for (int i = 0; i < jsonObject.getJSONArray("routes").length(); i++) {
			// System.out.println("Heyy i just found a route. Heyy i just found a route.
			// Heyy i just found a route. Heyy i just found a route. Heyy i just found a
			// route. Heyy i just found a route");
//			System.out.println(jsonObject.getJSONArray("routes").get(i).toString().charAt(2));
//			System.out.println(jsonObject.getJSONArray("routes").get(i).toString().charAt(6));
			characterArrayGrid[characterIndex
					.get(jsonObject.getJSONArray("routes").get(i).toString().charAt(2))][characterIndex
							.get(jsonObject.getJSONArray("routes").get(i).toString().charAt(6))] = 1;
			characterArrayGrid[characterIndex
					.get(jsonObject.getJSONArray("routes").get(i).toString().charAt(6))][characterIndex
							.get(jsonObject.getJSONArray("routes").get(i).toString().charAt(2))] = 1;
		}

		// for(int i=0;i<6;i ++) {
		// for(int j=0;j<6;j++) {
		// System.out.print(characterArrayGrid[i][j]);
		// }
		// System.out.println();
		// }

		if (characterArrayGrid[characterIndex.get(cityOne.charAt(0))][characterIndex.get(cityTwo.charAt(0))] == 1) {
			return true;
		} else {
//			for (int searchindex = 0; searchindex <characterArrayGrid[characterIndex.get(cityOne.charAt(0))].length; searchindex++) {
////				characterArrayGrid[characterIndex.get(cityOne.charAt(0))];
//				
//			}
			return this.finSecondPartRecursive(characterArrayGrid[characterIndex.get(cityOne.charAt(0))], characterIndex.get(cityTwo.charAt(0)), characterIndex.get(cityOne.charAt(0)));
		}

//		return false;
	}

	public static boolean finSecondPartRecursive(int[] arow, Integer indexToFind, int currentIndex) {
		System.out.println(currentIndex);
		storeOfChar.add(currentIndex);
		if(arow[indexToFind] == 1) {
			return true;
		} else {
			for(int i=0; i<arow.length; i++) {
				if(arow[i]==1 && i!= currentIndex && (storeOfChar.indexOf(i) == -1)) {
					System.out.println("next call is for index "+ i);
					return RouteDao.finSecondPartRecursive(characterArrayGrid[i], indexToFind, i);
				}
			}
			//return false;
		}
		
		return false;
	}
	
	public static void main(String[] args) throws JSONException, IOException {
		RouteDao routeDao = new RouteDao();
		boolean complexroute = routeDao.ifcomplexRouteExists("B", "D");
		if(complexroute) {
			System.out.println("Route exists");
		} else {
			System.out.println("route doesnt exists");
		}
//		String one = "A";
//		System.out.println("*************" + one.charAt(0));
	}

}
