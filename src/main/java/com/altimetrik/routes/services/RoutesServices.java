package com.altimetrik.routes.services;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetrik.router.dao.RouteDao;

@Service
public class RoutesServices {
	
	@Autowired
	RouteDao routeDao;
	public String routeExists(String cityOne, String cityTwo) throws JSONException {
		System.out.println("Rout exists method in services");
		try {
			boolean routeExists = routeDao.routeExists(cityOne, cityTwo);
			if(routeExists) return "Route Exists";
			else return "There is no Route between the two cities";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "route doesnt Exists";
	}
	
	public String complexRouteExists(String cityOne, String cityTwo) throws JSONException, IOException {
		System.out.println("Route for complex route in services");
		boolean complexRouteExists = routeDao.ifcomplexRouteExists(cityOne, cityTwo);
		
		if(complexRouteExists) return "Route Exists";
		else return "Route doesnt Exists";
	}
	
}
