package com.altimetrik.controller;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.routes.services.RoutesServices;

@RestController
public class RoutesController {

	@Autowired
	RoutesServices routeServices;
	
	
	@RequestMapping(value="/does-route-exists", method= RequestMethod.GET)
	public String routeExists(@RequestParam("from") String cityOne, @RequestParam("to") String cityTwo) throws JSONException {
		System.out.println("Route exists method in controller");
		return routeServices.routeExists(cityOne, cityTwo);
//		return "routeExists!!";
	}
	
	@RequestMapping(value="/complex-route-exists", method=RequestMethod.GET)
	public String complexRouteExists(@RequestParam("from") String cityOne, @RequestParam("to") String cityTwo) throws JSONException, IOException {
		 
		return routeServices.complexRouteExists(cityOne, cityTwo);
	}
	
	
}
